minetest.register_node("dylan:dylanbloc", {
    description = "Ceci est un bloc pour aller vite",
    tiles = {"dylan.png"},
    groups = { oddly_breakable_by_hand = 1},
    on_punch = function(pos, node,puncher, pointed_thing)
       local playerspeed = puncher:get_physics_override().speed
       if playerspeed > 1 then
            puncher:set_physics_override({
                speed = 1,
    })
        elseif playerspeed == 1 then
            puncher:set_physics_override({
                speed = 1000,
            })
        end
   end,
})